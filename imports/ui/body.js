import { Template } from 'meteor/templating';
 
import { Tasks } from '../api/tasks.js';

import '../ui/body.html';
 
Template.body.helpers({
  tasks(){
  	return Tasks.find({},{sort:{createdAt: -1}});
  }
});

Template.body.events({
	'submit .new-task'(event){
		//prevent default browser form submit
		event.preventDefault();

		//Get value from form element
		const target = event.target;
		const text = target.text.value;

		console.log(target);
		console.log(text);

		//Insert a task into the collection
		Task.insert({
			text,
			createdAt:new Date(),//current time
		});

		//Clear form
		target.text.value = '';

	}
});